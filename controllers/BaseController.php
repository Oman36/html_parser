<?php

namespace app\controllers;

use yii\web\Controller;


class BaseController extends Controller
{
    protected function renderJson($data)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $data;
    }
}
