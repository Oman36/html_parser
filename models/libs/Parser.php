<?php
namespace app\models\libs;
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: oman
 * Date: 24.12.16
 * Time: 13:11
 */
class Parser
{
    protected $_content;
    protected $_finalArray;
    protected $_errors = [];
    protected $_lastError;
    protected $_startDate;
    protected $_finishDate;
    protected $_data = [];
    protected $_balanceData = [];
    protected $_stepAmount = 20;
    protected $_timeStep;
    protected $_checkpoints =[];
    protected $_checkpointsDates =[];

    /**
     * Parser constructor.
     * @param $content
     */
    public function __construct($content = null)
    {
        ini_set('request_terminate_timeout', 300);
        ini_set('max_execution_time',300);

        if ($content) {
            $this->_content = $content;
            $this->readHTML($content);
        }
    }


    public function readHTML($content)
    {

        $doc = \phpQuery::newDocumentHTML($content);

        if (!$doc) {
            return $this->_addError('Контент не прочитан');
        }

        $table = $doc->find('table');

        if (!$table) {
            return $this->_addError('Таблица не найдена');
        }

        $rows = $table->find('tr');

        $elements = array_slice($rows->elements, 3);

        /**
         * @var  int $k
         * @var  \DOMElement $element
         */
        foreach ($elements as $k => $element) {
            $this->_parseRow($element);
        }

        $this->_countTimeStep()
            ->_fillCheckpoints();

        return $this;
    }

    protected function _countTimeStep()
    {
        $length = $this->getFinishDate() - $this->getStartDate();
        $this->_timeStep = ceil($length / $this->_stepAmount);

        return $this;
    }

    protected function _addError($message, $extra = [])
    {
        $this->_errors[] = [
            'message' => $message,
            'extra' => $extra,
        ];

        $this->_lastError = end($this->_errors);

        return $this;
    }

    protected function _parseRow(\DOMElement $element)
    {
        if ($element->childNodes->length < 3) {
            return $this;
        }

        if ('buy' === $element->childNodes->item(2)->nodeValue) {
            return $this->_parseBuy($element);
        }

        if ('balance' === $element->childNodes->item(2)->nodeValue) {
            return $this->_parseBalance($element);
        }

        return $this;
    }

    protected function _parseBalance($element)
    {
        $openDate = \DateTime::createFromFormat(
            'Y.m.d H:i:s',
            $element->childNodes->item(1)->nodeValue
        )->getTimestamp();

        $profit = floatval(
            str_replace(' ','',$element->childNodes->item(4)->nodeValue)
        );

        $this->_balanceData[] = [
            'date' => $openDate,
            'profit' => $profit,
        ];
        return $this;
    }

    /**
     * @param \DOMElement $element
     * @return $this
     */
    protected function _parseBuy($element)
    {
        $nodes = $element->childNodes;
        $openDate = \DateTime::createFromFormat(
            'Y.m.d H:i:s',
            $nodes->item(1)->nodeValue
        )->getTimestamp();

        $closeDate = \DateTime::createFromFormat(
            'Y.m.d H:i:s',
            $nodes->item(8)->nodeValue
        )->getTimestamp();

        $commission = floatval(str_replace(' ','',$nodes->item(10)->nodeValue));

        $profit = floatval(str_replace(' ','',$nodes->item(13)->nodeValue));

        $this->_data[] = [
            'openDate' => $openDate,
            'closeDate' => $closeDate,
            'commission' => $commission,
            'profit' => $profit,
        ];
        return $this;
    }

    protected function _fillCheckpoints()
    {
        $startDate = $this->getStartDate();

        foreach ($this->_data as $i => $transaction) {
            /* есть 3 части и 8 ключевых отметок времени
                        |первая |        вторая       | третья
              |__ ... __|____XXX|XXXXXXX| ... |XXXXXXX|XX_____|__ ... __|
              t0        t1  t2  t3                    t4 t5   t6        t7
              */

            // протяжоность все сделки в секундах t2-t5
            $time = $transaction['closeDate'] - $transaction['openDate'];

            $result = $transaction['profit'] + $transaction['commission'];

            if (!$time) {
                $time = 1;
            }

            // вообщем сколько шагов получилось
            $steps = $time / $this->getTimeStep();

            // Сколько пришло/ушло за каждый шаг
            $perStep = $result / $steps;

            // t3-t0 сколько прошло секунд с начала отсчета
            $missTime = $transaction['openDate'] - $startDate;

            // первый шаг, куда будем записывать
            $firstStep = intval(floor($missTime / $this->getTimeStep()));

            // отрезок t3-t2
            $timeFirstPart = $this->getTimeStep() - $missTime % $this->getTimeStep();

            if ($time < $timeFirstPart) {
                // если сюда попали, значит отрезок меньше шага
                // и полностью вошёл в шаг
                $this->_addToCheckpoint(
                    $firstStep,
                    $result
                );
                continue;
            }

            $this->_addToCheckpoint(
                $firstStep++,
                $perStep * $timeFirstPart / $this->getTimeStep()
            );

            $lostTime = $time - $timeFirstPart;

            // отрезок t3-t4
            while ($lostTime > $this->getTimeStep()) {
                $lostTime -= $this->getTimeStep();
                $this->_addToCheckpoint($firstStep++,$perStep);
            }

            // отрезок t4-t5
            $this->_addToCheckpoint(
                $firstStep,
                $perStep * $lostTime / $this->getTimeStep()
            );

        }

        foreach ($this->_balanceData as $i => $transaction) {

            $result = $transaction['profit'];

            $missTime = $transaction['date'] - $startDate;

            // шаг, куда будем записывать
            $step = intval(floor($missTime / $this->getTimeStep()));

            $this->_addToCheckpoint($step,$result);

        }

        return  $this;
    }

    protected function _addToCheckpoint($step,$value)
    {

        if (isset($this->_checkpoints[$step])) {
            $this->_checkpoints[$step]+= $value;
            return $this;
        }
        $this->_checkpoints[$step] = $value;
        return $this;
    }

    public function getData()
    {
        return $this->_data;
    }

    public function getBalanceData()
    {
        return $this->_balanceData;
    }

    public function getPreparedData()
    {
        $prepared = [];
        foreach ($this->_data as $item) {
            $prepared[] = [
                'open' => date('m.d H:i:s',$item['openDate']),
                'close' => date('m.d H:i:s',$item['closeDate']),
                'length' => $item['closeDate'] -$item['openDate'],
                'profit' => $item['profit'],
                'commission' => $item['commission'],
                'sum' => $item['profit'] + $item['commission'],
            ];
        }
        return $prepared;
    }

    public function getPreparedBalanceData()
    {
        $prepared = [];
        foreach ($this->_balanceData as $item) {
            $prepared[] = [
                'open' => date('m.d H:i:s',$item['date']),
                'sum' => $item['profit'],
            ];
        }
        return $prepared;
    }

    public function getStartDate()
    {
        if (!$this->_startDate ) {

            $dataDate = empty($this->_data[0]['openDate']) ?
                2147483647 : $this->_data[0]['openDate'];

            $dataBalanceDate = empty($this->_balanceData[0]['date']) ?
                2147483647 : $this->_balanceData[0]['date'];

            $this->_startDate = min($dataBalanceDate,$dataDate);
        }
        return $this->_startDate;
    }

    public function getFinishDate()
    {

        $dataDate = empty(end($this->_data)['closeDate']) ?
            -1 : end($this->_data)['closeDate'];

        $dataBalanceDate = empty(end($this->_balanceData)['date']) ?
            -1 : end($this->_balanceData)['date'];

        $this->_finishDate = max($dataBalanceDate,$dataDate);

        return $this->_finishDate;
    }

    public function getCheckpoints()
    {
        return $this->_checkpoints;
    }


    public function getTimeStep()
    {
        return $this->_timeStep;
    }

    public function getResultsInTime()
    {
        $maxStep = max(array_keys($this->_checkpoints));

        $array = [
            date('Y.m.d H:i:s',$this->_startDate) => $this->_checkpoints[0]
        ];

        for($i = 1; $i <= $maxStep; $i++) {
            $timestamp = $this->_startDate + ($i - 1) * $this->_timeStep;

            $array[date('Y.m.d H:i:s',$timestamp + $this->_timeStep)] =
                empty($this->_checkpoints[$i]) ?
                $array[date('Y.m.d H:i:s', $timestamp)] :
                $array[date('Y.m.d H:i:s', $timestamp)] +
                $this->_checkpoints[$i];
        }

        return $array;
    }
}