function GraphBuilder()
{
    this.form = $('#file_form');
    this.uploadFile = function (e) {
        e.preventDefault();
        var self = this;
        var data = new FormData($(this.form).get(0));
        var files = e.target.files;
        $.each(files, function(key, value)
        {
            data.append(key, value);
        });

        $.post({
            url : '/site/upload/',
            data : data,
            dataType : 'json',
            processData: false, // Не обрабатываем файлы (Don't process the files)
            contentType: false, // Так jQuery скажет серверу что это строковой запрос
            success : function (data) {
                self.form.find('.error').hide();
                new Chart($('#myChart'), {
                    type: 'line',
                    data: {
                        labels: data.checkpointsDates,
                        datasets: [{
                            label: 'USD',
                            data: data.checkpoints,
                            lineTension : 0,
                            borderWidth: 2
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                },
                            }]
                        }
                    }
                });
            },
            error : function (data) {
                self.form.find('.error')
                    .html('Ошибка при загрузке файла')
                    .fadeIn(400);
            }
        })
    };

    this.init = function () {
        var self = this;
        self.form
            .find('[name="file"]')
            .on('change',self.uploadFile.bind(self));
    }
}

$(document).ready(function () {
    (new GraphBuilder()).init();
});