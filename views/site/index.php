<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'My Yii Application';
\app\assets\ChartAsset::register($this);
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Html parser!</h1>

        <p class="lead">Let's load your file!</p>

        <?php $form = ActiveForm::begin(['id' => 'file_form','options' => ['enctype' => 'multipart/form-data']]); ?>


        <label class="btn btn-lg btn-success" >
            Click
            <span class="hidden">
                <input type="file" name="file">
            </span>
        </label>
        <br>
        <br>
        <div class="alert alert-danger error"></div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="body-content">
        <canvas id="myChart" width="400" height="200"></canvas>
    </div>
</div>
